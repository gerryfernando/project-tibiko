
const database = require('../config/config.js');

const express = require('express')

const router = express.Router()



const http = require('http');
const https = require('https');
const fetch = require('node-fetch');
const Bluebird = require('bluebird');
const mailjet = require('node-mailjet').connect('74ccbda9a0988e36219873fa797543af', '57e1c3170193b695f1fb75a56f23cb42');
const fs = require("fs");
fetch.Promise = Bluebird;


let generateFileToRecepient = function(username , email , orders){
	console.log(username);
	let file = fs.readFileSync("./TicketCinemakuy.html", "utf8");
	console.log(username);
	console.log(email);
	console.log(order);
	let listTicket = [];
	let totalPrice = 0;
	for(let i=0; i < orders.length;i++){
		let file2 = fs.readFileSync("./Ticket.html", "utf8");
		file2 = file2.replace("itemid0001" , orders[i].code );
		file2 = file2.replace("itemid0002" , moneyToCurency(orders[i].price) );
		file2 = file2.replace("itemid0003" , orders[i].bookCode );
		file2 = file2.replace("itemid0004" , orders[i].bookCode );
		totalPrice += orders[i].price;
		listTicket.push(file2);
	}
	file = file.replace("itemid0003" , listTicket)
	file = file.replace("itemid0004" , "Rp. "+moneyToCurency(totalPrice)+" ,-")
	file = file.replace("itemid0005" , getDate())
	file = file.replace("itemid0006" , orders.length+"")
	return file;
}

//token validator
var rand = function() {
    return Math.random().toString(36).substr(2); // remove `0.`
};

var bookingCode = function() {
    return rand(); // to make it longer
};


// Get movies that showing today
router.get('/movies/now_playing' , (req,res2) => {

	console.log(req.query);
	let url = 'https://api.gojekapi.com/v4/web/movie/now_playing?city='+(req.query.city||'Jakarta');
	console.log(url);
	https.get(url, (res) => { 
	  console.log('STATUS: ' + res.statusCode);

	  // Buffer the body entirely for processing as a whole.
	  let bodyChunks = [];
	  res.on('data', function(chunk) {
		// You can process streamed parts here...
		bodyChunks.push(chunk);
	  }).on('end', function() {
		let body = Buffer.concat(bodyChunks);
		// ...and/or process the entire body here.
		res2.send(JSON.parse(body));
	  })
	});
	
	
})
// Get name of all cities that available in database
router.get('/movies/cities' , (req,res2) => {
	let url = 'https://api.gojekapi.com/v1/web/events/cities';
	https.get(url, (res) => { 
	  console.log('STATUS: ' + res.statusCode);

	  // Buffer the body entirely for processing as a whole.
	  let bodyChunks = [];
	  res.on('data', function(chunk) {
		// You can process streamed parts here...
		bodyChunks.push(chunk);
	  }).on('end', function() {
		let body = JSON.parse(Buffer.concat(bodyChunks));
		
		if(body.data != null){
			body = body.data.cities;
		}
		// ...and/or process the entire body here.
		res2.send(body);
	  })
	});
	
	
})

// Get singgle data of a movies
router.get('/movies/sinopsis/:id' , (req,res2) => {

	let url = 'https://api.gojekapi.com/v1/web/movie/synopsis?event_id='+(req.params.id);
	console.log(url);
	https.get(url, (res) => { 
	  console.log('STATUS: ' + res.statusCode);

	  // Buffer the body entirely for processing as a whole.
	  let bodyChunks = [];
	  res.on('data', function(chunk) {
		// You can process streamed parts here...
		bodyChunks.push(chunk);
	  }).on('end', function() {
		let body = Buffer.concat(bodyChunks);
		// ...and/or process the entire body here.
		res2.send(JSON.parse(body));
	  })
	});
	
	
})

// Get cinema scedule for moview
router.get('/movies/schedule' , (req,res2) => {

	console.log(req.query);
	let url = 'https://api.gojekapi.com/v4/web/movie/cinemas?city='+(req.query.city||'Jakarta')+'&event_id='+req.query.event_id+'&date='+(req.query.date);
	console.log(url);
	https.get(url, (res) => { 
	  console.log('STATUS: ' + res.statusCode);

	  // Buffer the body entirely for processing as a whole.
	  let bodyChunks = [];
	  res.on('data', function(chunk) {
		// You can process streamed parts here...
		bodyChunks.push(chunk);
	  }).on('end', function() {
		let body = Buffer.concat(bodyChunks);
		// ...and/or process the entire body here.
		res2.send(JSON.parse(body));
	  })
	});
	
	
})
// Get cinema scedule for moview
router.get('/movies/schedule/:id/seats' , (req,res2) => {

	//let url = 'https://api.gojekapi.com/v4/web/movie/seat_layout?schedule_id='+req.params.id;
	let url = '/v4/web/movie/seat_layout?schedule_id='+req.params.id;
	console.log(url);
	
	getTokenGojekApi(function(dataxx){
		//console.log(dataxx);

		const options = {
		  hostname: 'api.gojekapi.com',
		  path: url,
		  headers: {
			'Content-Type': 'application/json; charset=utf-8',
			'access-token' : dataxx.access_token,
			'client' : dataxx.client,
			'uid' : dataxx.uid
		  }
		};

		https.get(options, (res) => { 
		  console.log('STATUS: ' + res.statusCode);

		  // Buffer the body entirely for processing as a whole.
		  let bodyChunks = [];
		  res.on('data', function(chunk) {
			// You can process streamed parts here...
			bodyChunks.push(chunk);
		  }).on('end', function() {
			let body = Buffer.concat(bodyChunks);
			// ...and/or process the entire body here.
			res2.send(JSON.parse(body));
		  })
		});
		
	});
	
	
})

// Make session for order seats
router.post('/movies/order' , (req,res2) => {

	let url = '/v3/web/movie/orders';
	//console.log(req.body);
	let orderBody =  OrderGenerator(req.body) ;
	console.log(orderBody);
	
	getTokenGojekApi(function(dataxx){
		console.log(dataxx);
		
		fetch('https://api.gojekapi.com/v3/web/movie/orders', {
			method: 'post',
			body:    JSON.stringify(orderBody),
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Sec-Fetch-Mode': 'cors',
				'Content-Type': 'application/json; charset=utf-8',
				'access-token' : dataxx.access_token,
				'client' : dataxx.client,
				'uid' : dataxx.uid
			  }
		})
		.then(res => res.json())
		.then(json => {
			console.log(json);
			res2.send(json);
		});
		
	});
			
});

//Buy Ticket using saldo

router.post('/movies/buyTicket' , (req,res2) => {
	let ref = database.ref("Account/"+req.body.username);
	respon = {isSuccess :false}
	ref.once('value').then(function(snapshot) {
			if(snapshot.val() != null){
				if(snapshot.val().auth.token!="" && snapshot.val().auth.token == req.body.token){
					let credit = snapshot.val().udata.credit;
					if(req.body.total <=credit){
						credit -= req.body.total;
						let list = {};
						for(let i = 0; i < req.body.orders.length; i++){
							req.body.orders[i].bookCode = req.body.schedule_id+'@'+req.body.orders[i].code+'@'+bookingCode();
							list['Ticket/'+req.body.schedule_id+'@'+req.body.orders[i].code] = {
								chair : req.body.orders[i].code,
								cinema : req.body.provider,
								date : req.body.date,
								price : req.body.orders[i].price,
								studio : req.body.audi,
								ticcode : req.body.orders[i].bookCode,
								uid : req.body.username,
								time: req.body.time
							}
						}
						list["Account/"+req.body.username+"/udata"] = {
							email:snapshot.val().udata.email,
							name:snapshot.val().udata.name,
							pass:snapshot.val().udata.pass,
							phone:snapshot.val().udata.phone,
							username:req.body.username,
							credit : credit
						};
						let isfinish = false;
						database.ref().update(list).then(() =>{
							// Data saved successfully!
							isfinish= true;
							respon.respon = "Purchased successfully"
							respon.isSuccess = true;
							console.log("Data saved successfully.",rand());
							//let file = generateFileToRecepient(req.body.username , snapshot.val().udata.email , req.body.orders[i] );
							let file = fs.readFileSync("./TicketCinemakuy.html", "utf8");
							let listTicket = [];
							let totalPrice = 0;
							for(let i=0; i < req.body.orders.length;i++){
								//console.log("test1-2");
								let file2 = fs.readFileSync("./Ticket.html", "utf8");
								file2 = file2.replace("itemid0001" , req.body.orders[i].code );
								file2 = file2.replace("itemid0002" , moneyToCurency(req.body.orders[i].price) );
								file2 = file2.replace("itemid0003" , req.body.orders[i].bookCode );
								file2 = file2.replace("itemid0004" , req.body.orders[i].bookCode );
								totalPrice += req.body.orders[i].price;
								listTicket.push(file2);
							}
							file = file.replace("itemid0003" , listTicket)
							//console.log("test1-2");
							file = file.replace("itemid0004" , "Rp. "+moneyToCurency(totalPrice)+" ,-")
							file = file.replace("itemid0005" , getDate())
							file = file.replace("itemid0006" , req.body.orders.length+"")
							//console.log("test2");
							let request = mailjet
								.post("send", {'version': 'v3.1'})
								.request({
								  "Messages":[
									{
									  "From": {
										"Email": "cinema.kuyorder@gmail.com",
										"Name": "CinemaKuy"
									  },
									  "To": [
										{
										  "Email": snapshot.val().udata.email,
										  "Name": req.body.username
										}
									  ],
									  "Subject": "CinemaKuy Ticket.",
									  "TextPart": "Ticket",
									  "HTMLPart": file,
									  "CustomID": "AppGettingStartedTest"
									}
								  ]
								});
							//console.log("test3");
							request
							  .then((result) => {
								console.log(result.body)
								res2.send(respon);
							  })
							  .catch((err) => {
								console.log(err.statusCode)
								respon.error = err;
								res2.send(respon);
							  })
							
							
							
							
							
							
							//res2.send(respon);
						}).catch((error)=>{
							
							respon.respon = "Failed When Input to database"
							respon.error = error;
							console.log("Data could not be saved");
							res2.send(respon);
							
						});
						
					}else{
						respon.respon = "Credit is not enough";
						res2.send(respon);
					}
				}else{
					respon.respon = "Can't Recognize User";
					res2.send(respon);
				}
			}else {
				respon.respon = "Getting account Failed, username doesn't exist";
				res2.send(respon);
			}
	});
	
	
})



// Make session for purchase seats
router.post('/movies/pay' , (req,res2) => {

	//console.log(req.body);
	let payBody =  {
						"order_id": req.body.order_id,
						"gender": 1,
						"payment_type": req.body.payment_provider_type,
						"data": [
							{
								"key": "encrypted_text",
								"value": "gotixvt2504350"
							}
						],
						"voucher_code": ""
					};
	console.log(payBody);
	
	getTokenGojekApi(function(dataxx){
		
		console.log(dataxx);
		
		fetch('https://api.gojekapi.com/v1/web/orders/'+payBody.order_id+'/purchase', {
			method: 'post',
			body:    JSON.stringify(payBody),
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Sec-Fetch-Mode': 'cors',
				'Content-Type': 'application/json; charset=utf-8',
				'access-token' : dataxx.access_token,
				'client' : dataxx.client,
				'uid' : dataxx.uid
			  }
		})
		.then(res => res.json())
		.then(json => {
			console.log(json);
			if(req.body.payment_provider_type == 4){
				sendEmail(req.body.username , req.body.email , json.data.payment_channel.gopay.qr_code , req.body.total_price , req.body.order_id );
			}else{
				res2.send(json);
			}
		});
		
	});
		
		
		
});

router.get('/movies/tes123' , (req,res) => {
	orders = [
	{
		code : "100",
		price : 10000,
		bookCode : "kjbibwe@jdsbvwdv@jobsdivs",
	},
	{
		code : "100",
		price : 10000,
		bookCode : "kjbibwe@jdsbvwdv@jobsdivs",
	},
	{
		code : "100",
		price : 10000,
		bookCode : "kjbibwe@jdsbvwdv@jobsdivs",
	},
	{
		code : "100",
		price : 10000,
		bookCode : "kjbibwe@jdsbvwdv@jobsdivs",
	},
	]
	let file = fs.readFileSync("./TicketCinemakuy.html", "utf8");
	
	let listTicket = [];
	let totalPrice = 0;
	for(let i=0; i < orders.length;i++){
		let file2 = fs.readFileSync("./Ticket.html", "utf8");
		file2 = file2.replace("itemid0001" , orders[i].code );
		file2 = file2.replace("itemid0002" , moneyToCurency(orders[i].price) );
		file2 = file2.replace("itemid0003" , orders[i].bookCode );
		file2 = file2.replace("itemid0004" , orders[i].bookCode );
		totalPrice += orders[i].price;
		listTicket.push(file2);
	}
	file = file.replace("itemid0003" , listTicket)
	file = file.replace("itemid0004" , "Rp. "+moneyToCurency(totalPrice)+" ,-")
	file = file.replace("itemid0005" , getDate())
	file = file.replace("itemid0006" , orders.length+"")
					
	res.send(file);
})

function moneyToCurency(money) {
	money = money+"";
	let extra = money.includes("-")?1:0;
	pointCount = (money.length-1-extra) / 3;
	let panjang = money.length;
	for(let i = 1 ; i <= pointCount; i++){
		let len = panjang - (3*i);
		money = money.slice(0, len) + "." + money.slice(len);
	}

	return money;
}


	

let sendTicketToEmail = function( username , email , orders  ){
	let file = fs.readFileSync("./TicketCinemakuy.html", "utf8");
	console.log(username);
	console.log(email);
	console.log(order);
	let listTicket = [];
	let totalPrice = 0;
	for(let i=0; i < orders.length;i++){
		let file2 = fs.readFileSync("./Ticket.html", "utf8");
		file2 = file2.replace("itemid0001" , orders[i].code );
		file2 = file2.replace("itemid0002" , moneyToCurency(orders[i].price) );
		file2 = file2.replace("itemid0003" , orders[i].bookCode );
		file2 = file2.replace("itemid0004" , orders[i].bookCode );
		totalPrice += orders[i].price;
		listTicket.push(file2);
	}
	file = file.replace("itemid0003" , listTicket)
	file = file.replace("itemid0004" , "Rp. "+moneyToCurency(totalPrice)+" ,-")
	file = file.replace("itemid0005" , getDate())
	file = file.replace("itemid0006" , orders.length+"")
	
	const request = mailjet
		.post("send", {'version': 'v3.1'})
		.request({
		  "Messages":[
			{
			  "From": {
				"Email": "cinema.kuyorder@gmail.com",
				"Name": "CinemaKuy"
			  },
			  "To": [
				{
				  "Email": email,
				  "Name": username
				}
			  ],
			  "Subject": "Gopay QR Code From CinemaKuy.",
			  "TextPart": "My first Mailjet email",
			  "HTMLPart": file,
			  "CustomID": "AppGettingStartedTest"
			}
		  ]
		})
		request
		  .then((result) => {
			console.log(result.body)
			//res.send(result.body);
		  })
		  .catch((err) => {
			console.log(err.statusCode)
			//res.send(err.statusCode);
		  })
}

function sendEmail( username , email , qr  , price , id ){
	let file = fs.readFileSync("./ReceiptCinemakuy.html", "utf8");
	file = file.replace("itemid0001" , "http://drive.google.com/uc?export=view&id=1a1DGCBSCAQoWVEaZQGs6XYY9XzKv7dPU")
	file = file.replace("itemid0002" , username)
	file = file.replace("itemid0003" , qr)
	file = file.replace("itemid0004" , "Rp. "+price+" ,-")
	file = file.replace("itemid0005" , getDate())
	file = file.replace("itemid0006" , id+" ")
	
	const request = mailjet
		.post("send", {'version': 'v3.1'})
		.request({
		  "Messages":[
			{
			  "From": {
				"Email": "cinema.kuyorder@gmail.com",
				"Name": "CinemaKuy"
			  },
			  "To": [
				{
				  "Email": email,
				  "Name": username
				}
			  ],
			  "Subject": "Gopay QR Code From CinemaKuy.",
			  "TextPart": "My first Mailjet email",
			  "HTMLPart": file,
			  "CustomID": "AppGettingStartedTest"
			}
		  ]
		})
		request
		  .then((result) => {
			//console.log(result.body)
			//res.send(result.body);
		  })
		  .catch((err) => {
			//console.log(err.statusCode)
			//res.send(err.statusCode);
		  })
}

// Get status of current purchase
router.get('/movies/pay/:id' , (req,res2) => {
	
	//let url = 'https://api.gojekapi.com/v4/web/movie/seat_layout?schedule_id='+req.params.id;
	let url = '/v1/web/orders/'+req.params.id+'/purchase_status';
	console.log(url);
	
	getTokenGojekApi(function(dataxx){
		//console.log(dataxx);

		const options = {
		  hostname: 'api.gojekapi.com',
		  path: url,
		  headers: {
			'Content-Type': 'application/json; charset=utf-8',
			'access-token' : dataxx.access_token,
			'client' : dataxx.client,
			'uid' : dataxx.uid
		  }
		};

		https.get(options, (res) => { 
		  console.log('STATUS: ' + res.statusCode);

		  // Buffer the body entirely for processing as a whole.
		  let bodyChunks = [];
		  res.on('data', function(chunk) {
			// You can process streamed parts here...
			bodyChunks.push(chunk);
		  }).on('end', function() {
			let body = Buffer.concat(bodyChunks);
			// ...and/or process the entire body here.
			res2.send(JSON.parse(body));
		  })
		});
		
	});
	
	
})

function OrderGenerator(base){
	result = {  
	   seats:"",
	   audi:base.audi,
	   type_of:"web",
	   voucher_id:"",
	   event_id:base.event_id,
	   tickets:[],
	   platform:"tixweb",
	   coordinate:"|",
	   is_point_voucher:false,
	   provider:base.provider
	}
	//console.log(base);
	base.seats.forEach((seat)=>{
		let newTicket = true;
		
		if(result.seats == ""){
			result.seats += seat.code;
		}else{
			result.seats += ";"+seat.code;
		}
		
		result.tickets.forEach((ticket)=>{
			if( ticket.ticket_id == seat.ticket_id ){
				ticket.quantity++;
				newTicket = false;
				//break;
			}
		},this)
		if(newTicket){
			result.tickets.push({quantity:1,ticket_id:seat.ticket_id});
		}
		
	},this)
	
	return result;
}


function getTokenGojekApi(callback){
	let ref = database.ref("apps-token");
	ref.once('value').then(function(snapshot) {
			callback(snapshot.val());
	});
	
}


function getDate() {

    var date = new Date();

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + "-" + month + "-" + day;

}

module.exports = router;
